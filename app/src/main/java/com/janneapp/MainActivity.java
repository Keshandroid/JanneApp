package com.janneapp;

import static android.os.Looper.getMainLooper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.GsonBuilder;
import com.janneapp.socket.UrlEndpoints;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.geojson.utils.PolylineUtils;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.MapboxConstants;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.OnRenderModeChangedListener;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.LineManager;
import com.mapbox.mapboxsdk.plugins.annotation.LineOptions;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;
import com.mapbox.mapboxsdk.utils.ColorUtils;
import com.mapbox.pluginscalebar.ScaleBarOptions;
import com.mapbox.pluginscalebar.ScaleBarPlugin;
import com.mapbox.turf.TurfJoins;
import com.mapbox.turf.TurfMeasurement;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import timber.log.Timber;

import static com.mapbox.mapboxsdk.style.layers.Property.LINE_JOIN_ROUND;
import static com.mapbox.mapboxsdk.style.layers.Property.NONE;
import static com.mapbox.mapboxsdk.style.layers.Property.VISIBLE;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;
import static com.mapbox.turf.TurfConstants.UNIT_METERS;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, PermissionsListener {

    private PermissionsManager permissionsManager;

    private static final String SEARCH_DATA_SYMBOL_LAYER_SOURCE_ID = "SEARCH_DATA_SYMBOL_LAYER_SOURCE_ID";
    private static final String FREEHAND_DRAW_LINE_LAYER_SOURCE_ID = "FREEHAND_DRAW_LINE_LAYER_SOURCE_ID";
    private static final String MARKER_SYMBOL_LAYER_SOURCE_ID = "MARKER_SYMBOL_LAYER_SOURCE_ID";
    private static final String FREEHAND_DRAW_FILL_LAYER_SOURCE_ID = "FREEHAND_DRAW_FILL_LAYER_SOURCE_ID";
    private static final String FREEHAND_DRAW_LINE_LAYER_ID = "FREEHAND_DRAW_LINE_LAYER_ID";
    private static final String FREEHAND_DRAW_FILL_LAYER_ID = "FREEHAND_DRAW_FILL_LAYER_ID";
    private static final String SEARCH_DATA_SYMBOL_LAYER_ID = "SEARCH_DATA_SYMBOL_LAYER_ID";
    private static final String SEARCH_DATA_MARKER_ID = "SEARCH_DATA_MARKER_ID";
    private static final String LINE_COLOR = "#a0861c";
    private static final float LINE_WIDTH = 5f;
    private static final float LINE_OPACITY = 1f;
    private static final float FILL_OPACITY = .4f;

    private boolean isCameraMoved = false;

    private MapView mapView;
    private MapboxMap mapboxMap;
    private FeatureCollection searchPointFeatureCollection;
    private List<Point> freehandTouchPointListForPolygon = new ArrayList<>();
    private List<Point> freehandTouchPointListForLine = new ArrayList<>();
    private boolean showSearchDataLocations = true;
    private boolean drawSingleLineOnly = false;

    //check gps status
    LocationManager locationManager ;
    boolean GpsStatus ;

    Location lastKnownLocation;
    private LocationEngine locationEngine;
    private static final long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private static final long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    private LocationChangeListeningActivityLocationCallback callback =
            new LocationChangeListeningActivityLocationCallback(this);


    //navigation drawers
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    ImageView leftDraweBtn,rightDraweBtn;
    NavigationView leftDrawer,rightDrawer;
    CardView cardSettings;

    private RelativeLayout mapZoomOut, mapZoomIn;
    private float zoomLevel = 0.0f;

    private TextView txtDistanceFromCenter;

    //symbols
    private SymbolManager symbolManager;
    private Symbol symbol;
//    private static final String MAKI_ICON_CAFE = "dog-park-15";
private static final String MAKI_ICON_ARROW = "triangle-15";

    private static final String MAKI_ICON_HARBOR = "harbor-15";

    //native socket io client
    private Socket mSocket;


    private CardView cardTopBar;
    private TextView txtDeviceBattery,txtSpeed,txtDistance;

    //polyline
    private List<Point> pointList = new ArrayList<>();
    private String lastDeviceLatitude="";
    private String lastDeviceLongitude="";
    private double totalDistance;
    private static final String ARROW_ICON_ID = "arrow-icon-id";

    //Timer
    private Chronometer cmtrArrivedTime;


    private View.OnTouchListener customOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            LatLng latLngTouchCoordinate = mapboxMap.getProjection().fromScreenLocation(
                    new PointF(motionEvent.getX(), motionEvent.getY()));

            Point screenTouchPoint = Point.fromLngLat(latLngTouchCoordinate.getLongitude(),
                    latLngTouchCoordinate.getLatitude());

            // Draw the line on the map as the finger is dragged along the map
            freehandTouchPointListForLine.add(screenTouchPoint);
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    GeoJsonSource drawLineSource = style.getSourceAs(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID);
                    if (drawLineSource != null) {
                        drawLineSource.setGeoJson(LineString.fromLngLats(freehandTouchPointListForLine));
                    }

                    // Draw a polygon area if drawSingleLineOnly == false
                    if (!drawSingleLineOnly) {
                        if (freehandTouchPointListForPolygon.size() < 2) {
                            freehandTouchPointListForPolygon.add(screenTouchPoint);
                        } else if (freehandTouchPointListForPolygon.size() == 2) {
                            freehandTouchPointListForPolygon.add(screenTouchPoint);
                            freehandTouchPointListForPolygon.add(freehandTouchPointListForPolygon.get(0));
                        } else {
                            freehandTouchPointListForPolygon.remove(freehandTouchPointListForPolygon.size() - 1);
                            freehandTouchPointListForPolygon.add(screenTouchPoint);
                            freehandTouchPointListForPolygon.add(freehandTouchPointListForPolygon.get(0));
                        }
                    }

                    // Create and show a FillLayer polygon where the search area is
                    GeoJsonSource fillPolygonSource = style.getSourceAs(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID);
                    List<List<Point>> polygonList = new ArrayList<>();
                    polygonList.add(freehandTouchPointListForPolygon);
                    Polygon drawnPolygon = Polygon.fromLngLats(polygonList);
                    if (fillPolygonSource != null) {
                        fillPolygonSource.setGeoJson(drawnPolygon);
                    }

                    // Take certain actions when the drawing is done
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                        // If drawing polygon, add the first screen touch point to the end of
                        // the LineLayer list so that it's
                        if (!drawSingleLineOnly) {
                            freehandTouchPointListForLine.add(freehandTouchPointListForPolygon.get(0));
                        }

                        if (showSearchDataLocations && !drawSingleLineOnly) {

                            // Use Turf to calculate the number of data points within a certain Polygon area
                            FeatureCollection pointsInSearchAreaFeatureCollection =
                                    TurfJoins.pointsWithinPolygon(searchPointFeatureCollection,
                                            FeatureCollection.fromFeature(Feature.fromGeometry(
                                                    drawnPolygon)));

                            // Create a Toast which say show many data points within a certain Polygon area
                            if (VISIBLE.equals(style.getLayer(
                                    SEARCH_DATA_SYMBOL_LAYER_ID).getVisibility().getValue())) {
                                Toast.makeText(MainActivity.this, String.format(
                                        getString(R.string.search_result_size),
                                        pointsInSearchAreaFeatureCollection.features().size()), Toast.LENGTH_SHORT).show();
                            }
                        }

                        if (drawSingleLineOnly) {
                            Toast.makeText(MainActivity.this,
                                    getString(R.string.move_map_drawn_line), Toast.LENGTH_SHORT).show();
                        }
                        enableMapMovement();
                    }
                }
            });

            return true;
        }
    };

    private Emitter.Listener onSocketConnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];

                    Log.d("LocationData","Connected....");


                    /*String username;
                    String message;
                    try {
                        username = data.getString("username");
                        message = data.getString("message");
                    } catch (JSONException e) {
                        return;
                    }*/

                    // add the message to view
                    //addPolyline(username, message);
                }
            });
        }
    };

    private Emitter.Listener onLocationData = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Log.d("LocationData",new GsonBuilder().setPrettyPrinting().create().toJson(args));

                        //reset timer
                        try {
                            cmtrArrivedTime.setBase(SystemClock.elapsedRealtime());

                            cmtrArrivedTime.start();
                            cmtrArrivedTime.setFormat("Last arrived location - %s");
                        }catch (Exception e){
                            e.printStackTrace();
                        }



                        JSONObject data = (JSONObject) args[0];

                        JSONObject locationData = data.getJSONObject("locationData");

                        String deviceLatitude = locationData.getString("lat");
                        String deviceLongitude = locationData.getString("long");
                        String deviceBattery = locationData.getString("battery");
                        String deviceDirection = locationData.getString("direction");
                        String speed = locationData.getString("speed");
                        String date = locationData.getString("date");

                        txtDeviceBattery.setText(deviceBattery);
                        txtSpeed.setText(speed + " km/h");
                        distanceBetweenDeviceAndCurrentLocation(deviceLatitude,deviceLongitude);

                        drawLine(deviceLatitude,deviceLongitude);



                    /*String username;
                    String message;
                    try {
                        username = data.getString("username");
                        message = data.getString("message");
                    } catch (JSONException e) {
                        return;
                    }*/

                        // add the message to view
                        //addPolyline(username, message);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });
        }
    };



    private void distanceBetweenDeviceAndCurrentLocation(String deviceLatitude, String deviceLongitude) {

        if(lastKnownLocation!=null){
//            LatLng currentLocation = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
//            LatLng deviceLocation = new LatLng(Double.parseDouble(deviceLatitude), Double.parseDouble(deviceLongitude));


            pointList.clear();
            pointList.add(Point.fromLngLat(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude())); // current location
            pointList.add(Point.fromLngLat(Double.parseDouble(deviceLongitude), Double.parseDouble(deviceLatitude)));// device location


            totalDistance = TurfMeasurement.distance(pointList.get(0),pointList.get(1),"kilometers");

            txtDistance.setText("Distance " + String.format("%.2f",totalDistance) + " km");

            Log.d("MainActivityLog"," ==CurrentLocation== " + "Longitude : " +lastKnownLocation.getLongitude() +
                                                              " Latitude : " + lastKnownLocation.getLatitude());
            Log.d("MainActivityLog"," ==DeviceLocation== " + "Longitude : " + Double.parseDouble(deviceLongitude) +
                                                              " Latitude : " + Double.parseDouble(deviceLatitude));
            Log.d("MainActivityLog"," ==TotalDistance== " + totalDistance);


        }


    }

    private void drawLine(String deviceLatitude, String deviceLongitude) {

        try {

            //mapboxMap.clear();


            //PolylineUtils positions = PolylineUtils.decode(attraction.getWaypoints(),5);


            if(lastDeviceLatitude!="" && lastDeviceLongitude!=""){
                LatLng[] latLng = new LatLng[2];
                latLng[0] = new LatLng(Double.parseDouble(lastDeviceLatitude),Double.parseDouble(lastDeviceLongitude));
                latLng[1] = new LatLng(Double.parseDouble(deviceLatitude),Double.parseDouble(deviceLongitude));

                //store device Lat-Long in local storage
                Identity.setDeviceLatitude(MainActivity.this, String.valueOf(Double.parseDouble(deviceLatitude)));
                Identity.setDeviceLongitude(MainActivity.this, String.valueOf(Double.parseDouble(deviceLongitude)));



                //to draw markers on map
//            mapboxMap.addMarker(new MarkerOptions().setPosition(new LatLng(Double.parseDouble(lastDeviceLatitude),Double.parseDouble(lastDeviceLongitude))));
//            mapboxMap.addMarker(new MarkerOptions().setPosition(new LatLng(Double.parseDouble(deviceLatitude),Double.parseDouble(deviceLongitude))));


                // Draw Points on MapView
                mapboxMap.addPolyline(new PolylineOptions()
                        .add(latLng)
                        .color(Color.parseColor("#38afea"))
                        .width(5));


                CameraPosition position = new CameraPosition.Builder()
                        .target(new LatLng(Double.parseDouble(deviceLatitude), Double.parseDouble(deviceLongitude)))
                        .zoom(20)
                        .bearing(bearingBetweenLocations(latLng[0],latLng[1]))
                        .build();





                if(!isCameraMoved){
                    isCameraMoved = true;
                    mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000);

                }




                //xml zoom
//                mapbox:mapbox_cameraTargetLat="35.087497"
//                mapbox:mapbox_cameraZoom="11.679132"
//                mapbox:mapbox_cameraTargetLng="-106.651261"






                //original
                if(symbolManager!=null){
                    symbolManager.deleteAll();
                }

                symbol = symbolManager.create(new SymbolOptions()
                        .withLatLng(new LatLng(Double.parseDouble(deviceLatitude), Double.parseDouble(deviceLongitude)))
                        .withIconImage(ARROW_ICON_ID)
                        .withIconSize(1.0f)
                        .withIconRotate(bearingBetweenLocations(latLng[0],latLng[1]))
                        .withDraggable(true));

                symbol.setIconImage(ARROW_ICON_ID);
                symbolManager.update(symbol);













            }

            lastDeviceLatitude = deviceLatitude;
            lastDeviceLongitude = deviceLongitude;


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public Bitmap getBitmap() {
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.arrow_up, null);
        Bitmap mBitmap = BitmapUtils.getBitmapFromDrawable(drawable);

        if (mBitmap != null && mBitmap.getConfig() != Bitmap.Config.ARGB_8888) {
            mBitmap = mBitmap.copy(Bitmap.Config.ARGB_8888, false);
        }
        return mBitmap;
    }

    float bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {
        double PI = 3.14159;
        double lat1 = latLng1.getLatitude() * PI / 180;
        double long1 = latLng1.getLongitude() * PI / 180;
        double lat2 = latLng2.getLatitude() * PI / 180;
        double long2 = latLng2.getLongitude() * PI / 180;
        double dLon = (long2 - long1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);
        double brng = Math.atan2(y, x);
        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return (float) brng;
    }

















    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Unable to connect to NodeJS server", Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_main);

        txtSpeed = findViewById(R.id.txtSpeed);
        txtDeviceBattery = findViewById(R.id.txtDeviceBattery);
        txtDistance = findViewById(R.id.txtDistance);
        cmtrArrivedTime = (Chronometer) findViewById(R.id.cmtrArrivedTime); // initiate a chronometer
        cardTopBar = findViewById(R.id.cardTopBar);

        //connect socket
        try {
            mSocket = IO.socket("http://173.249.36.203:8333");
        } catch (URISyntaxException e) {
            Log.d("LocationData","Exception");

            e.printStackTrace();
        }
        //mSocket.on(Socket.EVENT_CONNECT,onSocketConnect);



        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);

        mSocket.on(UrlEndpoints.LOCATION_DATA, onLocationData);
        mSocket.connect();




        //navigation drawers
//    toolbar = (Toolbar) findViewById(R.id.toolbarMain);
//    setSupportActionBar(toolbar);
//    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        leftDrawer = (NavigationView) findViewById(R.id.leftDrawer);
//    rightDrawer = (NavigationView) findViewById(R.id.rightDrawer);
        leftDraweBtn = (ImageView) findViewById(R.id.imgBtnLeftDrawer);
        rightDraweBtn = (ImageView) findViewById(R.id.imgBtnRightDrawer);
        mapZoomOut = (RelativeLayout) findViewById(R.id.mapZoomOut);
        mapZoomIn = (RelativeLayout) findViewById(R.id.mapZoomIn);
        txtDistanceFromCenter = (TextView) findViewById(R.id.txtDistanceFromCenter);

        cardSettings = (CardView) findViewById(R.id.cardSettings);

//    drawerLayout.addDrawerListener(actionBarDrawerToggle);

        leftDraweBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawerLayout.openDrawer(GravityCompat.START);


        /*if(drawerLayout.isDrawerOpen(leftDrawer)){
            drawerLayout.closeDrawer(leftDrawer);
          }else if(drawerLayout.isDrawerOpen(leftDrawer)){
            drawerLayout.openDrawer(leftDrawer);
          }

          if(drawerLayout.isDrawerOpen(rightDrawer)){
            drawerLayout.closeDrawer(rightDrawer);
          }*/
            }
        });

        rightDraweBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawerLayout.openDrawer(GravityCompat.END);


        /*if(drawerLayout.isDrawerOpen(rightDrawer)){
          drawerLayout.closeDrawer(rightDrawer);
        }else if(drawerLayout.isDrawerOpen(rightDrawer)){
          drawerLayout.openDrawer(rightDrawer);
        }

        if(drawerLayout.isDrawerOpen(leftDrawer)){
          drawerLayout.closeDrawer(leftDrawer);
        }*/
            }
        });


        cardSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,TerrainMapActivity.class));
            }
        });

        cardTopBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Identity.getDeviceLatitude(MainActivity.this)!= ""){
                    CameraPosition position = new CameraPosition.Builder()
                            .target(new LatLng(Double.parseDouble(Identity.getDeviceLatitude(MainActivity.this)), Double.parseDouble(Identity.getDeviceLongitude(MainActivity.this))))
                            .zoom(20)
                            .build();

                    if(mapboxMap!=null){
                        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000);
                    }



                }

            }
        });






        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull final MapboxMap mapboxMap) {


                //other stype Style.LIGHT
                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        MainActivity.this.mapboxMap = mapboxMap;


                        symbolManager = new SymbolManager(mapView, mapboxMap, style);

                        style.addImage(ARROW_ICON_ID, BitmapUtils.getBitmapFromDrawable(
                                getResources().getDrawable(R.drawable.arrow_up)));

                        //Set Scalebar
                        ScaleBarPlugin scaleBarPlugin = new ScaleBarPlugin(mapView, mapboxMap);

                        // Create a ScaleBarOptions object to use the Plugin's default styling
                        scaleBarPlugin.create(new ScaleBarOptions(MainActivity.this));



                        //display old markers
                        if (showSearchDataLocations) {
                            new LoadGeoJson(MainActivity.this).execute();
                        } else {
                            setUpExample(null);
                        }

                        findViewById(R.id.clear_map_for_new_draw_fab)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        // Reset ArrayLists
                                        freehandTouchPointListForPolygon = new ArrayList<>();
                                        freehandTouchPointListForLine = new ArrayList<>();

                                        // Add empty Feature array to the sources
                                        GeoJsonSource drawLineSource = style.getSourceAs(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID);
                                        if (drawLineSource != null) {
                                            drawLineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
                                        }

                                        GeoJsonSource fillPolygonSource = style.getSourceAs(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID);
                                        if (fillPolygonSource != null) {
                                            fillPolygonSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
                                        }

                                        enableMapDrawing();
                                    }
                                });

                        findViewById(R.id.switch_to_single_line_only_fab)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        drawSingleLineOnly = !drawSingleLineOnly;
                                        Toast.makeText(MainActivity.this, String.format(
                                                getString(R.string.now_drawing), drawSingleLineOnly ? getString(R.string.single_line) :
                                                        getString(R.string.polygon)), Toast.LENGTH_SHORT).show();
                                    }
                                });

                        findViewById(R.id.currentLocationBtn)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        lastKnownLocation = mapboxMap.getLocationComponent().getLastKnownLocation();

                                        mapboxMap.moveCamera(CameraUpdateFactory.newLatLng(
                                                new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude())));
                                    }
                                });

                        findViewById(R.id.currentLocationBtn)
                                .setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {

                                        CheckGpsStatus();

                                        return true;
                                    }
                                });



                        mapZoomOut.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                mapboxMap.setCameraPosition(new CameraPosition.Builder()
                                        .zoom(MapboxConstants.MINIMUM_ZOOM)
                                        .build());
                            }
                        });

                        mapZoomIn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                /*mapboxMap.setCameraPosition(new CameraPosition.Builder()
                                        .zoom(MapboxConstants.MAXIMUM_ZOOM)
                                        .build());*/
                            }
                        });


                    }
                });




            }

        });



    }



    public void CheckGpsStatus(){
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    /*if(GpsStatus == true) {
      textview.setText("GPS Is Enabled");
    } else {
      textview.setText("GPS Is Disabled");
    }*/
        Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent1);

    }

    /**
     * Enable moving the map
     */
    private void enableMapMovement() {
        mapView.setOnTouchListener(null);
    }

    /**
     * Enable drawing on the map by setting the custom touch listener on the {@link MapView}
     */
    private void enableMapDrawing() {
        mapView.setOnTouchListener(customOnTouchListener);
    }

    private void setUpExample(FeatureCollection searchDataFeatureCollection) {

        searchPointFeatureCollection = searchDataFeatureCollection;

        mapboxMap.getStyle(new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style loadedStyle) {
                /*loadedStyle.addImage(SEARCH_DATA_MARKER_ID, BitmapFactory.decodeResource(
                        MainActivity.this.getResources(), R.drawable.blue_marker_view));*/

                // Add sources to the map
                loadedStyle.addSource(new GeoJsonSource(SEARCH_DATA_SYMBOL_LAYER_SOURCE_ID,
                        searchDataFeatureCollection));
                loadedStyle.addSource(new GeoJsonSource(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID));
                loadedStyle.addSource(new GeoJsonSource(MARKER_SYMBOL_LAYER_SOURCE_ID));
                loadedStyle.addSource(new GeoJsonSource(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID));

                loadedStyle.addLayer(new SymbolLayer(SEARCH_DATA_SYMBOL_LAYER_ID,
                        SEARCH_DATA_SYMBOL_LAYER_SOURCE_ID).withProperties(
                        iconImage(SEARCH_DATA_MARKER_ID),
                        iconAllowOverlap(true),
                        iconOffset(new Float[] {0f, -8f}),
                        iconIgnorePlacement(true))
                );

                // Add freehand draw LineLayer to the map
                loadedStyle.addLayerBelow(new LineLayer(FREEHAND_DRAW_LINE_LAYER_ID,
                        FREEHAND_DRAW_LINE_LAYER_SOURCE_ID).withProperties(
                        lineWidth(LINE_WIDTH),
                        lineJoin(LINE_JOIN_ROUND),
                        lineOpacity(LINE_OPACITY),
                        lineColor(Color.parseColor(LINE_COLOR))), SEARCH_DATA_SYMBOL_LAYER_ID
                );

                // Add freehand draw polygon FillLayer to the map
                loadedStyle.addLayerBelow(new FillLayer(FREEHAND_DRAW_FILL_LAYER_ID,
                        FREEHAND_DRAW_FILL_LAYER_SOURCE_ID).withProperties(
                        fillColor(Color.RED),
                        fillOpacity(FILL_OPACITY)), FREEHAND_DRAW_LINE_LAYER_ID
                );

                //use this to enable map drawing free hand when app opens
//                enableMapDrawing();

                //enable current location
                enableLocationComponent(loadedStyle);


                findViewById(R.id.show_search_data_points_fab).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showSearchDataLocations = !showSearchDataLocations;

                        // Toggle the visibility of the fake data point SymbolLayer icons
                        Layer dataLayer = loadedStyle.getLayer(SEARCH_DATA_SYMBOL_LAYER_ID);
                        if (dataLayer != null) {
                            dataLayer.setProperties(
                                    VISIBLE.equals(dataLayer.getVisibility().getValue()) ? visibility(NONE) : visibility(VISIBLE));
                        }
                    }
                });

                Toast.makeText(MainActivity.this,
                        getString(R.string.draw_instruction), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @SuppressWarnings( {"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            // Get an instance of the component
            LocationComponent locationComponent = mapboxMap.getLocationComponent();

            // Activate with options
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build());

            // Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

            // Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);


            initLocationEngine();

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        LocationEngineRequest request = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(request, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });




        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        MainActivity.this.mapboxMap = mapboxMap;

        mapboxMap.setStyle(Style.MAPBOX_STREETS,
                new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);
                    }
                });
    }

    /**
     * Use an AsyncTask to retrieve GeoJSON data from a file in the assets folder.
     */
    private static class LoadGeoJson extends AsyncTask<Void, Void, FeatureCollection> {

        private WeakReference<MainActivity> weakReference;

        LoadGeoJson(MainActivity activity) {
            this.weakReference = new WeakReference<>(activity);
        }

        @Override
        protected FeatureCollection doInBackground(Void... voids) {
            try {
                MainActivity activity = weakReference.get();
                if (activity != null) {
                    InputStream inputStream = activity.getAssets().open("albuquerque_locations.geojson");
                    return FeatureCollection.fromJson(convertStreamToString(inputStream));
                }
            } catch (Exception exception) {
                Timber.e("Exception Loading GeoJSON: %s", exception.toString());
            }
            return null;
        }

        static String convertStreamToString(InputStream is) {
            Scanner scanner = new Scanner(is).useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }


        @Override
        protected void onPostExecute(@Nullable FeatureCollection featureCollection) {
            super.onPostExecute(featureCollection);
            MainActivity activity = weakReference.get();
            if (activity != null && featureCollection != null) {
                activity.setUpExample(featureCollection);
            }
        }
    }

    // Add the mapView lifecycle to the activity's lifecycle methods
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();

        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    private static class LocationChangeListeningActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<MainActivity> activityWeakReference;

        LocationChangeListeningActivityLocationCallback(MainActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        /**
         * The LocationEngineCallback interface's method which fires when the device's location has changed.
         *
         * @param result the LocationEngineResult object which has the last known location within it.
         */
        @Override
        public void onSuccess(LocationEngineResult result) {
            MainActivity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                // Create a Toast which displays the new location's coordinates
               /* Toast.makeText(activity, String.format(activity.getString(R.string.new_location),
                                String.valueOf(result.getLastLocation().getLatitude()),
                                String.valueOf(result.getLastLocation().getLongitude())),
                        Toast.LENGTH_SHORT).show();
*/


                Log.d("lastKnownLocation",""+result.getLastLocation().getLatitude() + " " + result.getLastLocation().getLongitude());


                activity.lastKnownLocation = result.getLastLocation();



                // Pass the new location to the Maps SDK's LocationComponent
                if (activity.mapboxMap != null && result.getLastLocation() != null) {
                    activity.mapboxMap.getLocationComponent().forceLocationUpdate(result.getLastLocation());

                }
            }
        }

        /**
         * The LocationEngineCallback interface's method which fires when the device's location can't be captured
         *
         * @param exception the exception message
         */
        @Override
        public void onFailure(@NonNull Exception exception) {
            MainActivity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

}
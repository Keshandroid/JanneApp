package com.janneapp.socket;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.janneapp.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.Polling;
import io.socket.engineio.client.transports.PollingXHR;
import io.socket.engineio.client.transports.WebSocket;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;

/**
 * This class will handle on socket base operation and we will assign listener to all socket events
 */

public class SocketHandler extends BaseActivity {

    Context mContext;
    static public SocketListener socketListener = null;
    String TAG = SocketHandler.class.toString();

    public SocketHandler(Context mContext) {
        this.mContext = mContext;
    }

    public SocketListener getSocketListener() {
        return socketListener;
    }

    public void setSocketListener(SocketListener socketListener) {
        SocketHandler.socketListener = socketListener;
    }

    public void connectToSocket() {
        UrlEndpoints.isSocketConnecting = true;
        try {

            HostnameVerifier myHostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            TrustManager[] trustAllCerts= new TrustManager[] { new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }};

            SSLContext mySSLContext = null;
            try {
                //mySSLContext = SSLContext.getInstance("TLS"); // original
                mySSLContext = SSLContext.getInstance("SSL"); // new
                try {
                    mySSLContext.init(null, trustAllCerts, null); // original
                    //mySSLContext.init(null, trustAllCerts, new java.security.SecureRandom()); // new
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }

            // default settings for all sockets <<original>>
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .hostnameVerifier(myHostnameVerifier)
                    //.sslSocketFactory(mySSLContext.getSocketFactory())
                    .connectTimeout(0,TimeUnit.MILLISECONDS)
                    .readTimeout(0,TimeUnit.MILLISECONDS)
                    .writeTimeout(0,TimeUnit.MILLISECONDS)
                    .build();

            IO.setDefaultOkHttpWebSocketFactory(okHttpClient);
            IO.setDefaultOkHttpCallFactory(okHttpClient);


            //String[] transport = {"websocket", "polling"};


            //2.0.1 working
            IO.Options options = new IO.Options();
            options.callFactory = okHttpClient;
            options.webSocketFactory = okHttpClient;
            options.reconnection = true;
            UrlEndpoints.socketIOClient = IO.socket(URI.create(UrlEndpoints.SOCKET_URL), options);


            /*URI uri = URI.create(UrlEndpoints.SOCKET_URL);
            IO.Options options = IO.Options.builder()
                    .setReconnection(true)
                    .setForceNew(true)
                    //.setTransports(new String[]{WebSocket.NAME})
                    .setUpgrade(false)
                    .setReconnectionAttempts(Integer.MAX_VALUE)
                    .setReconnectionDelayMax(5000L)
                    .setReconnectionDelay(1000L)
                    .setTransports(new String[]{Polling.NAME})
                    .build();
                    UrlEndpoints.socketIOClient = IO.socket(uri, options);*/

            /*URI uri = URI.create(UrlEndpoints.SOCKET_URL);
            IO.Options options = new IO.Options();
            options.webSocketFactory = okHttpClient;
            options.callFactory = okHttpClient;
            //options.transports = new String[]{Polling.NAME};

            UrlEndpoints.socketIOClient = IO.socket(uri, options);*/


            UrlEndpoints.socketIOClient.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (socketListener != null) {
                        Log.v("SocketHandler", "connected...");
                        socketListener.isSocketConnected(true);
                    }
                }
            })/*.on(Manager.EVENT_RECONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... arg0) {

                    Log.v("SocketHandler", "RECONNECT MSG(0)::::"+arg0[0].toString());
                    Log.v("SocketHandler", "RECONNECT MSG(size)::::"+arg0.length);
                }
            })*/.on(Manager.EVENT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... arg0) {

                    Log.v("SocketHandler", "call manager::::"+arg0[0].toString());
                    Log.v("SocketHandler", "call manager::::"+arg0.length);
                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (socketListener != null) {
                        Log.v("SocketHandler", "disconnected");

                        socketListener.isSocketConnected(false);
                    }
                }
            }).on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.v("SocketHandler", "Transport Error::::"+args.length);
                    Log.v("SocketHandler", "Transport Error::::"+args[0].toString());
                }
            }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.v("SocketHandler", "call Error::::"+args.length);
                    Log.v("SocketHandler", "call Error::::"+args[0].toString());
                }
            }).on(UrlEndpoints.LOCATION_DATA, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (socketListener != null) {
                                    Log.d("SocketEvent","Location-Data");

                            }
                        }
                    });
                }
            });

            UrlEndpoints.socketIOClient.connect();

        } catch (Exception e) {
            Log.d("SocketHandler"," Exception : "+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

}

package com.janneapp;

import android.content.Context;
import android.net.ConnectivityManager;

public class Utils {

    public static boolean isInternetConnected(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null;
        } catch (Exception e2) {
            return false;
        }
    }

}

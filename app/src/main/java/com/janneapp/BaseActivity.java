package com.janneapp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.janneapp.socket.SocketHandler;
import com.janneapp.socket.SocketListener;
import com.janneapp.socket.UrlEndpoints;

import org.json.JSONObject;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import io.socket.client.Manager;
import io.socket.client.Socket;

public class BaseActivity extends AppCompatActivity implements SocketListener {

    //socket-start
    public boolean isSocketDisconnectManage = true;
    private ObjectMapper mapper = null;
    private static final Lock lock = new ReentrantLock();
    JSONObject jsonObject;
    String events;
    private SocketHandler socketHandler;
    public static boolean isAppWentToBg = false;
    public static boolean isWindowFocused = false;
    public static boolean isBackPressed = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //socket init
        initView();

    }

    private void initView() {
        //realm = Realm.getDefaultInstance();

        if (UrlEndpoints.socketIOClient != null) {
            if (UrlEndpoints.socketIOClient.connected()) {
                socketHandler = new SocketHandler(BaseActivity.this);
                socketHandler.setSocketListener(BaseActivity.this);
            } else {
                getSocketIoClient();
            }
        } else {
            getSocketIoClient();
        }
    }

    public Socket getSocketIoClient() {
        if (UrlEndpoints.socketIOClient == null && !UrlEndpoints.isSocketConnecting) {
            connectSocket();
        } else if (UrlEndpoints.socketIOClient != null) {
            if (!UrlEndpoints.socketIOClient.connected()) {
                connectSocket();
            }
        } else {
            socketHandler = new SocketHandler(BaseActivity.this);
            socketHandler.setSocketListener(BaseActivity.this);
        }
        return UrlEndpoints.socketIOClient;
    }

    public void connectSocket() {
        try {

            SocketHandler socketHandler = new SocketHandler(BaseActivity.this);
            socketHandler.setSocketListener(BaseActivity.this);
            if (Utils.isInternetConnected(BaseActivity.this)) {
                socketHandler.connectToSocket();
            }


        } catch (Exception e) {

            Log.d("socketConnect9", "" + e.toString());

            e.printStackTrace();
        }
    }

    @Override
    public void isSocketConnected(boolean connected) {
        Log.v("SocketBaseActivity", "isSocketConnected:" + connected);

    }

    @Override
    public void isSocketReConnected() {
        /*jsonObject = new JSONObject();
        events = Manager.EVENT_RECONNECT;

        uiHandler.post(runnableSocket);*/
    }

    @Override
    public void onEvent(String event, Object... arg0) {
       /* events = event;
        jsonObject = (JSONObject) arg0[0];

        uiHandler.post(runnableSocket);*/
    }

    /*Handler uiHandler = new Handler(Looper.getMainLooper());

    public Runnable runnableSocket = new Runnable() {
        @Override
        public void run() {
            if (events != null && jsonObject != null) {
                if (chatEventListener != null) {
                    chatEventListener.onSocketEvent(events, jsonObject);
                } else {
                    parseResponseinMain(events, jsonObject);
                }
            }
        }
    };*/

}

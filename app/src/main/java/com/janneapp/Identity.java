package com.janneapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Identity {

    private static final String DEVICE_LATITUDE = "DEVICE_LATITUDE";
    private static final String DEVICE_LONGITUDE = "DEVICE_LONGITUDE";


    public static String getDeviceLatitude(Context mContext) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return mPreferences.getString(DEVICE_LATITUDE, "");
    }

    public static void setDeviceLatitude(Context mContext, String localeKey) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mPreferences.edit().putString(DEVICE_LATITUDE, localeKey).apply();
    }

    public static String getDeviceLongitude(Context mContext) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return mPreferences.getString(DEVICE_LONGITUDE, "");
    }

    public static void setDeviceLongitude(Context mContext, String localeKey) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mPreferences.edit().putString(DEVICE_LONGITUDE, localeKey).apply();
    }



}
